package integration;

import marker.IntegrationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.amster.tm.endpoint.*;
import ru.amster.tm.service.WebServiceLocator;

@Category(IntegrationTest.class)
@Ignore
public class TaskTest {

    private static TaskEndpoint taskEndpoint;

    private static SessionEndpoint sessionEndpoint;

    private static String session, session1;

    @BeforeClass
    public static void init() {
        WebServiceLocator webServiceLocator = new WebServiceLocator();
        taskEndpoint = webServiceLocator.getTaskEndpoint();
        sessionEndpoint = webServiceLocator.getSessionEndpoint();

        session = sessionEndpoint.openSession("test", "test");
        session1 = sessionEndpoint.openSession("admin", "admin");

        ProjectEndpoint projectEndpoint = webServiceLocator.getProjectEndpoint();
        projectEndpoint.createWithTwoParamProject(session, "project");
        projectEndpoint.createWithTwoParamProject(session1, "project1");

        taskEndpoint.createWithTwoParamTask(session, "demo", "project");
        taskEndpoint.createWithTwoParamTask(session, "test", "project");
        taskEndpoint.createWithTwoParamTask(session, "temp", "project");

        taskEndpoint.createWithThreeParamTask(session1, "demo", "session1", "project1");
    }

    @Test
    @Category(IntegrationTest.class)
    public void findAndRemoveTaskTest() {
        TaskDTO task = taskEndpoint.findTaskByName(session, "demo");
        Assert.assertEquals("demo", task.getName());
        Assert.assertNotEquals("session1", task.getDescription());

        TaskDTO task1 = taskEndpoint.findTaskByName(session, "test");
        Assert.assertEquals(task1.getName(), "test");

        taskEndpoint.removeTaskByName(session, "demo");
        TaskDTO task5 = taskEndpoint.findTaskByName(session1, "demo");
        Assert.assertEquals("demo", task5.getName());
        Assert.assertEquals("session1", task5.getDescription());
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskCreateTaskException() {
        taskEndpoint.createWithTwoParamTask("adsedqwd2132dad/sadasda/sdas89yhuhqd", "ds", "ds");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskShowByNameTaskException() {
        taskEndpoint.findTaskByName("adsedqwd2132dad/sadasda/sdas89yhuhqd", "demo");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskRemoveByNameTaskException() {
        taskEndpoint.removeTaskByName("adsedqwd2132dad/sadasda/sdas89yhuhqd", "demo");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskCreateTaskNullException() {
        taskEndpoint.createWithTwoParamTask(session, null, "ds");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskShowByNameTaskNullException() {
        taskEndpoint.findTaskByName(session, null);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskRemoveByNameTaskNullException() {
        taskEndpoint.removeTaskByName(session, null);
    }


    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskCreateTaskEmptyException() {
        taskEndpoint.createWithTwoParamTask(session, "", "ds");
    }
    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskShowByNameTaskEmptyException() {
        taskEndpoint.findTaskByName(session, "");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskRemoveByNameTaskEmptyException() {
        taskEndpoint.removeTaskByName(session, "");
    }

    @AfterClass
    public static void exit() {
        taskEndpoint.removeTaskByName(session, "test");
        taskEndpoint.removeTaskByName(session, "temp");
        taskEndpoint.removeTaskByName(session1, "demo");
        sessionEndpoint.closeSession(session1);
        sessionEndpoint.closeSession(session);
    }

}