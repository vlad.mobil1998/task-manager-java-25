package integration;

import marker.IntegrationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.amster.tm.endpoint.*;
import ru.amster.tm.service.WebServiceLocator;

@Category(IntegrationTest.class)
@Ignore
public class UserTest {

    private static WebServiceLocator webServiceLocator;

    private static UserEndpoint userEndpoint;

    private static SessionEndpoint sessionEndpoint;

    private static String session, session1;

    @BeforeClass
    public static void init() {
        webServiceLocator = new WebServiceLocator();
        userEndpoint = webServiceLocator.getUserEndpoint();
        sessionEndpoint = webServiceLocator.getSessionEndpoint();
        session = sessionEndpoint.openSession("test", "test");
        session1 = sessionEndpoint.openSession("admin", "admin");
    }

    @Test
    public void findUser() {
        UserDTO user = userEndpoint.findUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());

        UserDTO user1 = userEndpoint.findUser(session1);
        Assert.assertNotNull(user1);
        Assert.assertEquals("admin", user1.getLogin());
    }

    @Test
    public void userUpdateProfile() {
        userEndpoint.updateUserEmail(session, "test@test.com");
        UserDTO user1 = userEndpoint.findUser(session1);
        Assert.assertNotNull(user1);
        Assert.assertNotEquals(user1.getEmail(), "test@test.com");

        userEndpoint.updateUserFirstName(session1, "tester");
        UserDTO user3 = userEndpoint.findUser(session);
        Assert.assertNotNull(user3);
        Assert.assertNotEquals(user3.getFistName(), "tester");

        userEndpoint.updateUserMiddleName(session, "testim");
        UserDTO user5 = userEndpoint.findUser(session1);
        Assert.assertNotNull(user5);
        Assert.assertNotEquals(user5.getMiddleName(), "testim");

        userEndpoint.updateUserLastName(session1, "testLast");
        UserDTO user7 = userEndpoint.findUser(session);
        Assert.assertNotNull(user7);
        Assert.assertNotEquals(user7.getLastName(), "testLast");
    }

    @Test(expected = RuntimeException.class)
    public void userCreateAndRemoveTest() {
        userEndpoint.createWithThreeParamUser("temp", "temp", Role.USER);

        String session = sessionEndpoint.openSession("temp", "temp");
        userEndpoint.updateUserPassword(session, "temp1");
        sessionEndpoint.closeSession(session);
        session = null;
        session = sessionEndpoint.openSession("temp", "temp1");
        Assert.assertNotNull(session);

        userEndpoint.removeUser(session);
        String session1 = sessionEndpoint.openSession("temp", "temp");
        Assert.assertNull(session1);
    }

    @AfterClass
    public static void exit() {
        sessionEndpoint.closeSession(session);
        sessionEndpoint.closeSession(session1);
    }

}