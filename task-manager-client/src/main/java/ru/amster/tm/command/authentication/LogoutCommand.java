package ru.amster.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.SessionEndpoint;
import ru.amster.tm.exception.user.AccessDeniedException;

public final class LogoutCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "logout";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Log out system";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @NotNull final SessionEndpoint sessionEndpoint = webServiceLocator.getSessionEndpoint();
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();
        sessionEndpoint.closeSession(webServiceLocator.getSession());
        webServiceLocator.setSession(null);
        System.out.println("[OK]");
    }

}