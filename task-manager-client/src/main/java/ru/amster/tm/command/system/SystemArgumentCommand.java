package ru.amster.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;

import java.util.Collection;

public final class SystemArgumentCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "arguments";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Display arguments program";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> arguments = webServiceLocator.getCommandService().getArguments();
        for (AbstractCommand argument : arguments) {
            System.out.println(argument.arg());
        }
    }

}