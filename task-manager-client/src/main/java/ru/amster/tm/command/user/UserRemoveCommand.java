package ru.amster.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.user.AccessDeniedException;

public final class UserRemoveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "re-user";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Delete user by login";
    }

    @Override
    public void execute() {
        System.out.println("[DELETED USER]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        @NotNull final UserEndpoint userEndpoint = webServiceLocator.getUserEndpoint();
        userEndpoint.removeUser(webServiceLocator.getSession());
        System.out.println("[OK]");
    }

}