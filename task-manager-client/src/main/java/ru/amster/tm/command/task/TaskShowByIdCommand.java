package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.TaskDTO;
import ru.amster.tm.endpoint.TaskEndpoint;
import ru.amster.tm.endpoint.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TaskUtil;
import ru.amster.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-v-id";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show task by id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        System.out.println("ENTER ID");
        @Nullable final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final TaskEndpoint taskEndpoint = webServiceLocator.getTaskEndpoint();
        @Nullable final TaskDTO task = taskEndpoint.findTaskById(webServiceLocator.getSession(), id);
        if (task == null) throw new EmptyTaskException();
        TaskUtil.showTask(task);
        System.out.println("[OK]");
    }

}