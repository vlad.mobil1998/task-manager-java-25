package ru.amster.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;

import java.util.Collection;

public final class SystemCommandCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "commands";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Display terminal command";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = webServiceLocator.getCommandService().getCommands();
        for (AbstractCommand command : commands) {
            System.out.println(command.name());
        }
    }

}