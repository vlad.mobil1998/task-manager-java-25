package ru.amster.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import lombok.experimental.UtilityClass;
import ru.amster.tm.endpoint.ProjectDTO;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.endpoint.Project;

@UtilityClass
public class ProjectUtil {

    @NotNull
    public void showProject(@Nullable final ProjectDTO project) {
        if (project == null) throw new EmptyProjectException();
        System.out.println("ID: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

}