package ru.amster.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IService;
import java.util.List;

@NoArgsConstructor
public abstract class Service<E> implements IService {

    @Nullable
    @Override
    public abstract List<E> findAll();

    @Override
    public abstract void add(@Nullable final Object record);

    @Override
    public abstract void clear();

}