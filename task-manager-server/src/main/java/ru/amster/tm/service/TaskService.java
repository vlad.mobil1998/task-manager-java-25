package ru.amster.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Task;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyEntityException;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.repository.Repository;
import ru.amster.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class TaskService extends Service<Task> implements ITaskService {

    @NotNull
    private IServiceLocator serviceLocator;

    public TaskService(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String projectId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = serviceLocator.getProjectService().findOneById(userId, projectId);
        User user = serviceLocator.getUserService().findById(userId);
        @NotNull final Task task = new Task(name, project, user);
        add(task);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (projectId == null || projectId.isEmpty())
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        Project project = serviceLocator.getProjectService().findOneById(userId, projectId);
        User user = serviceLocator.getUserService().findById(userId);
        @NotNull final Task task = new Task(name, project, user);
        task.setDescription(description);
        add(task);
    }

    @NotNull
    @Override
    public Task findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        ITaskRepository taskRepository = new TaskRepository(em);
        Task task = taskRepository.findOneByName(userId, name);
        em.close();
        if (task == null) throw new EmptyTaskException();
        return task;
    }

    @NotNull
    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        ITaskRepository taskRepository = new TaskRepository(em);
        Task task = taskRepository.findOneById(userId, id);
        if (task == null) throw new EmptyTaskException();
        return task;
    }

    @NotNull
    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        ITaskRepository taskRepository = new TaskRepository(em);
        Integer count = taskRepository.removeOneByName(userId, name);
        if (count > 1) {
            em.getTransaction().rollback();
            em.close();
            throw new InvalidIndexException();
        }
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        ITaskRepository taskRepository = new TaskRepository(em);
        taskRepository.removeOneById(userId, id);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public void updateTaskById(
            @Nullable final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        ITaskRepository taskRepository = new TaskRepository(em);
        @Nullable final Task task = taskRepository.findOneById(userId, id);
        if (task == null) throw new EmptyTaskException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        em.merge(task);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public Integer numberOfAllTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        ITaskRepository taskRepository = new TaskRepository(em);
        Integer count = taskRepository.numberOfAllTasks(userId);
        em.getTransaction().commit();
        em.close();
        return count;
    }

    @Override
    public void load(@Nullable final List<Task> tasks) {
        clear();
        for (@NotNull Task task : tasks) {
            add(task);
        }
    }

    @NotNull
    public List<Task> export() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        ITaskRepository taskRepository = new TaskRepository(em);
        List<Task> tasks = taskRepository.getEntity();
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        ITaskRepository repository = new TaskRepository(em);
        List<Task> record = repository.getEntity();
        em.close();
        return record;
    }

    @Override
    public void add(@Nullable final Object record) {
        if (record == null) throw new EmptyEntityException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        ITaskRepository repository = new TaskRepository(em);
        repository.merge(record);
        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void clear() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        ITaskRepository repository = new TaskRepository(em);
        repository.removeAll();
        em.getTransaction().commit();
        em.close();
    }

}