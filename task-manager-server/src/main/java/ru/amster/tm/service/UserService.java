package ru.amster.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.exception.empty.*;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.repository.UserRepository;
import ru.amster.tm.util.HashUtil;

import javax.jws.soap.SOAPBinding;
import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class UserService extends Service implements IUserService {

    IServiceLocator serviceLocator;

    public UserService(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        add(user);
    }

    @NotNull
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = new User();
        if (user == null) throw new EmptyUserException();
        user.setLogin(login);
        user.setEmail(email);
        user.setPasswordHash(HashUtil.salt(password));
        add(user);
    }

    @NotNull
    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        if (user == null) throw new EmptyUserException();
        user.setLogin(login);
        user.setEmail(email);
        user.setRole(role);
        user.setPasswordHash(HashUtil.salt(password));
        add(user);
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IUserRepository userRepository = new UserRepository(em);
        User user = userRepository.findById(id);
        em.close();
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IUserRepository userRepository = new UserRepository(em);
        User user = userRepository.findByLogin(login);
        em.close();
        return user;
    }

    @NotNull
    @Override
    public void removeUser(@Nullable final User user) {
        if (user == null) throw new EmptyUserException();
        removeById(user.getId());
    }

    @NotNull
    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IUserRepository userRepository = new UserRepository(em);
        userRepository.removeById(id);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IUserRepository userRepository = new UserRepository(em);
        Integer count = userRepository.removeByLogin(login);
        if (count > 1) {
            em.getTransaction().rollback();
            em.close();
            throw new InvalidIndexException();
        }
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public void updateEmail(@NotNull final String id, @Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        user.setEmail(email);
        em.merge(user);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public void updateFirstName(@NotNull final String id, @Nullable final String firstName) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        user.setFistName(firstName);
        em.merge(user);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public void updateLastName(@NotNull final String id, @Nullable final String lastName) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        user.setLastName(lastName);
        em.merge(user);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public void updateMiddleName(@NotNull final String id, @Nullable final String middleName) {
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        user.setMiddleName(middleName);
        em.merge(user);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public void updatePassword(@NotNull final String id, @Nullable final String password) {
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        @NotNull final String passwordUpd = HashUtil.salt(password);
        user.setPasswordHash(passwordUpd);
        em.merge(user);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        user.setLocked(true);
        em.merge(user);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EmptyUserException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        user.setLocked(false);
        em.merge(user);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void load(@NotNull final List<User> users) {
        clear();
        for (@NotNull User user : users) {
            add(user);
        }
    }

    @NotNull
    @Override
    public List<User> export() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IUserRepository userRepository = new UserRepository(em);
        List<User> users = userRepository.getEntity();
        em.close();
        return users;
    }

    @Nullable
    @Override
    public List<User> findAll() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IUserRepository repository = new UserRepository(em);
        List<User> record = repository.getEntity();
        em.close();
        return record;
    }

    @Override
    public void add(@Nullable final Object record) {
        if (record == null) throw new EmptyEntityException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IUserRepository repository = new UserRepository(em);
        repository.merge(record);
        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void clear() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IUserRepository repository = new UserRepository(em);
        repository.removeAll();
        em.getTransaction().commit();
        em.close();
    }

}