package ru.amster.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyEntityException;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class ProjectService extends Service<Project> implements IProjectService {

    @NotNull
    private IServiceLocator serviceLocator;

    public ProjectService(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        Project project = findOneByName(userId, name);
        if (project != null) return;
        project = new Project(name, user);
        add(project);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        Project project = findOneByName(userId, name);
        if (project != null) return;
        project = new Project(name, user);
        project.setDescription(description);
        add(project);
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IProjectRepository projectRepository = new ProjectRepository(em);
        Project project = projectRepository.findOneById(userId, id);
        em.close();
        if (project == null) return null;
        return project;
    }

    @Nullable
    @Override
    public Project findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IProjectRepository projectRepository = new ProjectRepository(em);
        Project project = projectRepository.findOneByName(userId, name);
        em.close();
        if (project == null) return null;
        return project;
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IProjectRepository projectRepository = new ProjectRepository(em);
        Integer count = projectRepository.removeOneByName(userId, name);
        if (count > 1) {
            em.getTransaction().rollback();
            em.close();
            throw new InvalidIndexException();
        } else if (count < 1 || count == null) throw new EmptyProjectException();
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IProjectRepository projectRepository = new ProjectRepository(em);
        projectRepository.removeOneById(userId, id);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public void updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IProjectRepository projectRepository = new ProjectRepository(em);
        @Nullable final Project project = projectRepository.findOneById(userId, id);
        if (project == null) throw new EmptyProjectException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        em.merge(project);
        em.getTransaction().commit();
        em.close();
    }

    @Nullable
    @Override
    public Integer numberOfAllProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IProjectRepository projectRepository = new ProjectRepository(em);
        Integer count = projectRepository.numberOfAllProjects(userId);
        em.close();
        return count;
    }

    @Override
    public void load(@NotNull final List<Project> projects) {
        clear();
        for (@NotNull Project project : projects) {
            add(project);
        }
    }

    @NotNull
    public List<Project> export() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IProjectRepository projectRepository = new ProjectRepository(em);
        List<Project> projects = projectRepository.getEntity();
        em.close();
        return projects;
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IProjectRepository repository = new ProjectRepository(em);
        List<Project> record = repository.getEntity();
        em.close();
        return record;
    }

    @Override
    public void add(@Nullable final Object record) {
        if (record == null) throw new EmptyEntityException();
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IProjectRepository repository = new ProjectRepository(em);
        repository.merge(record);
        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void clear() {
        EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        em.getTransaction().begin();
        IProjectRepository repository = new ProjectRepository(em);
        repository.removeAll();
        em.getTransaction().commit();
        em.close();
    }

}