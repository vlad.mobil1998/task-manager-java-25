package ru.amster.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.dto.TaskDTO;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.Task;
import ru.amster.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@WebService
public final class TaskEndpoint {

    IServiceLocator serviceLocator;

    public TaskEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Nullable
    public List<TaskDTO> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(sessionSecret);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        List<Task> tasks = taskService.findAll();
        List<TaskDTO> taskDTOList = new ArrayList<>();
        for (Task task: tasks) {
            TaskDTO taskDTO = new TaskDTO(task);
            taskDTOList.add(taskDTO);
        }
        return taskDTOList;
    }

    @WebMethod
    public void createWithTwoParamTask(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String projectName
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        Project project = projectService.findOneByName(session.getUser().getId(), projectName);
        taskService.create(session.getUser().getId(), project.getId(), name);
    }

    @WebMethod
    public void createWithThreeParamTask(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String projectName
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        Project project = projectService.findOneByName(session.getUser().getId(), projectName);
        taskService.create(session.getUser().getId(), project.getId(), name, description);
    }

    @WebMethod
    @Nullable
    public TaskDTO findTaskById(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        return new TaskDTO(taskService.findOneById(session.getUser().getId(), id));
    }

    @WebMethod
    @Nullable
    public TaskDTO findTaskByName(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        return new TaskDTO(taskService.findOneByName(session.getUser().getId(), name));
    }

    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.removeOneByName(session.getUser().getId(), name);
    }

    @WebMethod
    public void removeTaskById(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.removeOneById(session.getUser().getId(), id);
    }

    @WebMethod
    public void updateTaskById(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.updateTaskById(session.getUser().getId(), id, name, description);
    }

    @WebMethod
    @NotNull
    public Integer numberOfAllTasks(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret
    )  {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        return taskService.numberOfAllTasks(session.getUser().getId());
    }

}