package ru.amster.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import ru.amster.tm.dto.ProjectDTO;
import ru.amster.tm.dto.UserDTO;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@WebService
public final class ProjectEndpoint {

    IServiceLocator serviceLocator;

    public ProjectEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public void createWithTwoParamProject(
            @WebParam(name = "sessionSecret", partName = "sessionSecret") @Nullable final String sessionSecret,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.create(session.getUser().getId(), name);
    }

    @WebMethod
    public List<ProjectDTO> findAllProject(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        sessionService.validate(sessionSecret);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        List<Project> projects =  projectService.findAll();
        List<ProjectDTO> projectDTOList = new ArrayList<>();
        for (Project project: projects) {
            ProjectDTO projectDTO = new ProjectDTO(project);
            projectDTOList.add(projectDTO);
        }
        return projectDTOList;
    }

    @WebMethod
    public void createWithThreeParamProject(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.create(session.getUser().getId(), name, description);
    }

    @WebMethod
    @Nullable
    public ProjectDTO findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return new ProjectDTO(projectService.findOneById(session.getUser().getId(), id));
    }

    @WebMethod
    @Nullable
    public ProjectDTO findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return new ProjectDTO(projectService.findOneByName(session.getUser().getId(), name));
    }

    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.removeOneByName(session.getUser().getId(), name);
    }

    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.removeOneById(session.getUser().getId(), id);
    }

    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.updateProjectById(session.getUser().getId(), id, name, description);
    }

    @WebMethod
    @NotNull
    public Integer numberOfAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final String sessionSecret
    ) {
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        Session session = sessionService.validate(sessionSecret);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return projectService.numberOfAllProjects(session.getUser().getId());
    }

}