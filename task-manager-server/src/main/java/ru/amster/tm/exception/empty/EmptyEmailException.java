package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public final class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("ERROR! Email is empty...");
    }

}