package ru.amster.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.amster.tm.api.repository.IRepository;

import java.util.List;

@NoArgsConstructor
public abstract class Repository<E> implements IRepository {

    @Override
    public abstract void merge(@NotNull Object record);

    @Override
    public abstract void removeAll();

    @NotNull
    @Override
    public abstract List<E> getEntity();

    @Override
    public abstract void remove(@NotNull Object record);


}