package ru.amster.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.entity.Task;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyLoginException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;


public final class UserRepository extends Repository<User> implements IUserRepository {

    @NotNull private final EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        try {
            @NotNull final TypedQuery<User> query = entityManager.createQuery(
                    "SELECT e FROM User e WHERE " +
                            "e.id = :id",
                    User.class
            );
            query.setParameter("id", id);
            @Nullable final User result = query.getSingleResult();
            return result;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyIdException();
        try {
            @NotNull final TypedQuery<User> query = entityManager.createQuery(
                    "SELECT e FROM User e WHERE " +
                            "e.login = :login",
                    User.class
            );
            query.setParameter("login", login);
            @Nullable final User result = query.getSingleResult();
            return result;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @Nullable
    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyLoginException();
        entityManager.remove(entityManager.find(User.class, id));
    }

    @Nullable
    @Override
    public Integer removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull  final Query query = entityManager.createQuery(
                "DELETE FROM User e WHERE " +
                        "e.login = :login"
        );
        query.setParameter("login", login);
        return query.executeUpdate();
    }

    @Override
    public void merge(@NotNull final Object record) {
        entityManager.merge(record);
    }

    @Override
    public void removeAll() {
        List<User> users = getEntity();
        for (User user: users) {
            entityManager.remove(user);
        }
    }

    @NotNull
    @Override
    public List<User> getEntity() {
        TypedQuery<User> query = entityManager.createQuery("FROM User", User.class);
        return query.getResultList();
    }

    @Override
    public void remove(@NotNull Object record) {
        entityManager.createQuery("DELETE FROM User e WHERE Object = :record")
                .setParameter("record", record).executeUpdate();
    }

}