package ru.amster.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.user.AccessDeniedException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;


public final class TaskRepository extends Repository<Task> implements ITaskRepository {

    private EntityManager entityManager;

    public TaskRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        try {
            @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                    "SELECT e FROM Task e WHERE " +
                            "e.user.id = :userId",
                    Task.class);
            query.setParameter("userId", userId);
            @Nullable final List<Task> result = query.getResultList();
            return result;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                "DELETE e FROM Task e WHERE " +
                        "e.user.id = :userId",
                Task.class
        );
        query.setParameter("userId", userId);
        query.executeUpdate();
    }

    @Nullable
    @Override
    public Task findOneByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        try {
            @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                    "SELECT e FROM Task e WHERE " +
                            "e.user.id = :userId and " +
                            "e.name = :name",
                    Task.class
            );
            query.setParameter("userId", userId);
            query.setParameter("name", name);
            @Nullable final Task result = query.getSingleResult();
            return result;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        try {
            @NotNull final TypedQuery<Task> query = entityManager.createQuery(
                    "SELECT e FROM Task e WHERE " +
                            "e.user.id = :userId and " +
                            "e.id = :id",
                    Task.class
            );
            query.setParameter("userId", userId);
            query.setParameter("id", id);
            @Nullable final Task result = query.getSingleResult();
            return result;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @NotNull
    @Override
    public Integer removeOneByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull  final Query query = entityManager.createQuery(
                "DELETE FROM Task e WHERE " +
                        "e.user.id = :userId and " +
                        "e.name = :name"
        );
        query.setParameter("userId", userId);
        query.setParameter("name", name);
        return query.executeUpdate();
    }

    @NotNull
    @Override
    public Integer removeOneById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Query query = entityManager.createQuery(
                "DELETE FROM Task e WHERE " +
                        "e.user.id = :userId and " +
                        "e.id = :id"
        );
        query.setParameter("userId", userId);
        query.setParameter("id", id);
        return query.executeUpdate();
    }

    @NotNull
    @Override
    public Integer numberOfAllTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final TypedQuery<Integer> query = entityManager.createQuery(
                "SELECT COUNT Task e FROM e.user.id = :userId",
                Integer.class
        );
        query.setParameter("userId", userId);
        return query.getSingleResult();
    }

    @Override
    public void merge(@NotNull final Object record) {
        Task task = findOneByName(((Task)record).getUser().getId(), ((Task)record).getName());
        if (task == null) entityManager.persist(record);
    }

    @Override
    public void removeAll() {
        List<Task> tasks = getEntity();
        for(Task task: tasks) {
            entityManager.remove(task);
        }
    }

    @NotNull
    @Override
    public List<Task> getEntity() {
        TypedQuery<Task> query = entityManager.createQuery("FROM e Task", Task.class);
        return query.getResultList();
    }

    @Override
    public void remove(@NotNull Object record) {
        entityManager.createQuery("DELETE FROM Task e WHERE Object = :record")
                .setParameter("record", record).executeUpdate();
    }

}