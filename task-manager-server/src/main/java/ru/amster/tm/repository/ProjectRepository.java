package ru.amster.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public final class ProjectRepository extends Repository<Project> implements IProjectRepository {

    @NotNull
    private final EntityManager entityManager;

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        try {
            @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                    "SELECT e FROM Project e WHERE " +
                    "e.user.id = :userId",
                    Project.class);
            query.setParameter("userId", userId);
            @Nullable final List<Project> result = query.getResultList();
            return result;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                "DELETE e FROM Project e WHERE " +
                "e.user.id = :userId",
                Project.class
        );
        query.setParameter("userId", userId);
        query.executeUpdate();
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        try {
            @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                    "SELECT e FROM Project e WHERE " +
                    "e.user.id = :userId and " +
                    "e.id = :id",
                    Project.class
            );
            query.setParameter("userId", userId);
            query.setParameter("id", id);
            @Nullable final Project result = query.getSingleResult();
            return result;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @Nullable
    @Override
    public Project findOneByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        try {
           @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                    "SELECT e FROM Project e WHERE " +
                    "e.user.id = :userId and " +
                    "e.name = :name",
                    Project.class
            );
            query.setParameter("userId", userId);
            query.setParameter("name", name);
            @Nullable final Project result = query.getSingleResult();
            return result;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @NotNull
    @Override
    public Integer removeOneByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull  final Query query = entityManager.createQuery(
                "DELETE FROM Project e WHERE " +
                "e.user.id = :userId and " +
                "e.name = :name"
        );
        query.setParameter("userId", userId);
        query.setParameter("name", name);
        return query.executeUpdate();
    }

    @Override
    public void removeOneById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        try {
            entityManager.remove(entityManager.find(Project.class, id));
        } catch (RuntimeException e) {
            throw new EmptyProjectException();
        }
    }

    @NotNull
    @Override
    public Integer numberOfAllProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final TypedQuery<Integer> query = entityManager.createQuery(
                "SELECT e COUNT Project e FROM e.user.id = :userId",
                Integer.class
        );
        query.setParameter("userId", userId);
        return query.getSingleResult();
    }

    @Override
    public void merge(@NotNull final Object record) {
        Project project = entityManager.find(Project.class, ((Project)record).getUser().getId());
        if (project == null) entityManager.persist(record);
    }

    @Override
    public void removeAll() {
        List<Project> projects = getEntity();
        for (Project project: projects) {
            entityManager.remove(project);
        }
    }

    @NotNull
    @Override
    public List<Project> getEntity() {
        TypedQuery<Project> query = entityManager.createQuery("FROM  Project", Project.class);
        return query.getResultList();
    }

    @Override
    public void remove(@NotNull Object record) {
        entityManager.createQuery("DELETE FROM Project e WHERE Object = :record")
                .setParameter("record", record).executeUpdate();
    }

}