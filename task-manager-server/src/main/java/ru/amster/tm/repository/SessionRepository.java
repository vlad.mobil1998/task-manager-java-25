package ru.amster.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.ISessionRepository;
import ru.amster.tm.entity.Session;
import ru.amster.tm.exception.user.AccessDeniedException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;


public class SessionRepository extends Repository<Session> implements ISessionRepository {

    private EntityManager entityManager;

    public SessionRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public List<Session> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        try {
            @NotNull final TypedQuery<Session> query = entityManager.createQuery(
                    "SELECT e FROM Session e WHERE " +
                            "e.user.id = :userId",
                    Session.class
            );
            query.setParameter("userId", userId);
            @Nullable final List<Session> result = query.getResultList();
            return result;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @Override
    public Integer removeByUserId(@NotNull final String userId) {
        if (userId == null ||userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final Query query = entityManager.createQuery(
                "DELETE FROM Session e WHERE " +
                        "e.user.id = :userId"
        );
        query.setParameter("userId", userId);
        return query.executeUpdate();
    }

    @Override
    public Boolean contains(@NotNull String id) {
            @NotNull final TypedQuery<Long> query = entityManager.createQuery(
                    "SELECT COUNT (*) FROM Session e WHERE e.id = :id",
                    Long.class
            );
            query.setParameter("id", id);
            Long result = query.getSingleResult();
            if (result == 1) return true;
            return false;
    }

    @Override
    public void merge(@NotNull final Object record) {
        entityManager.merge(record);
    }

    @Override
    public void removeAll() {
        List<Session> sessions = getEntity();
        for (Session session: sessions) {
            entityManager.remove(session);
        }
    }

    @NotNull
    @Override
    public List<Session> getEntity() {
        TypedQuery<Session> query = entityManager.createQuery("FROM Session", Session.class);
        return query.getResultList();
    }

    @Override
    public void remove(@NotNull Object record) {
        entityManager.createQuery("DELETE FROM Session e WHERE Object = :record")
                .setParameter("record", record).executeUpdate();
    }

}