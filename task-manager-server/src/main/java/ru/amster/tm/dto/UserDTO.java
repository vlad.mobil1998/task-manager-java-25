package ru.amster.tm.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.User;

import javax.persistence.Column;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Data
public final class UserDTO {

    @NotNull
    private String id;

    @NotNull
    private String login;

    @Nullable
    private String email;

    @Nullable
    private String fistName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private Role role = Role.USER;

    @Nullable
    @Column(nullable = false)
    private Boolean Locked = false;

    public UserDTO(@Nullable final User user) {
        if (user == null) return;
        id = user.getId();
        login = user.getLogin();
        email = user.getEmail();
        fistName = user.getFistName();
        lastName = user.getLastName();
        middleName = user.getMiddleName();
        role = user.getRole();
    }

}