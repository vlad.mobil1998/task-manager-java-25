package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Project;
import ru.amster.tm.repository.Repository;

import java.util.List;

public interface IProjectRepository extends IRepository {

    @Nullable
    List<Project> findAll(@NotNull String userId);

    void removeAll(String userId);

    @NotNull
    Project findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Integer removeOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    void removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Integer numberOfAllProjects(@NotNull String userId);

}