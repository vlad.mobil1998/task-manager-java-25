package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.User;

import java.util.List;

public interface IUserService extends IService {

    void create(@Nullable String login, @Nullable String password, @Nullable Role role);

    void create(@Nullable String login, @Nullable String password, @Nullable String email);

    void create(@Nullable String login, @Nullable String password, @Nullable String email, @Nullable Role role);

    @Nullable
    User findById(@Nullable String id);

    @NotNull
    User findByLogin(@Nullable String login);

    @NotNull
    void removeUser(@Nullable User user);

    @NotNull
    void removeById(@Nullable String id);

    @NotNull
    void removeByLogin(@Nullable String login);

    @NotNull
    void updateEmail(@NotNull String id, @Nullable String email);

    @NotNull
    void updateFirstName(@NotNull String id, @Nullable String firstName);

    @NotNull
    void updateLastName(@NotNull String id, @Nullable String lastName);

    @NotNull
    void updateMiddleName(@NotNull String id, @Nullable String middleName);

    @NotNull
    void updatePassword(@NotNull String id, @Nullable String password);

    @NotNull
    void lockUserByLogin(@Nullable String login);

    @NotNull
    void unlockUserByLogin(@Nullable String login);

    void load(List<User> users);

    @NotNull
    List<User> export();

}