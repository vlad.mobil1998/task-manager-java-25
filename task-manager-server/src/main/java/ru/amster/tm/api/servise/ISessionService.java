package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Session;

public interface ISessionService extends IService {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    String open(@NotNull String login, @NotNull String password);

    void close(@NotNull String sessionSecret);

    Session validate(@Nullable String session);

    Session sign(@NotNull Session session);

    Session validate(@NotNull String session, @Nullable Role role);

    void signOutByLogin(@Nullable String login);

    void signOutByUserId(@Nullable String userId);
}
