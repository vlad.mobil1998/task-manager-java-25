package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.User;

public interface IUserRepository extends IRepository {

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    void removeById(@NotNull String id);

    @NotNull
    Integer removeByLogin(@NotNull String login);

}