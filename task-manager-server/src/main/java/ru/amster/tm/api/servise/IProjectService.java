package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project findOneByName(@Nullable String userId, @Nullable String name);

    void removeOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    void removeOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    void updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Integer numberOfAllProjects(@Nullable String userId);

    void load(@Nullable List<Project> projects);

    @NotNull
    List<Project> export();

}