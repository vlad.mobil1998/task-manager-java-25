package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<E> {

    @Nullable
    List<E> findAll();

    void add(@NotNull Object record);

    void clear();

}