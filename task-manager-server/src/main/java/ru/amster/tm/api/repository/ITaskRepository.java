package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository {

    @NotNull
    List<Task> findAll(@Nullable String userId);

    void removeAll(@Nullable String userId);

    @Nullable
    Task findOneById(@Nullable String userId, @NotNull String id);

    @Nullable
    Task findOneByName(@Nullable String userId, @NotNull String name);

    @NotNull
    Integer removeOneByName(@Nullable String userId, @NotNull String name);

    @NotNull
    Integer removeOneById(@Nullable String userId, @NotNull String id);

    @NotNull
    Integer numberOfAllTasks(@Nullable String userId);

}