package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IRepository<E> {

    void merge(@NotNull Object record);

    void removeAll();

    @NotNull List getEntity();

    void remove(@NotNull Object record);

}