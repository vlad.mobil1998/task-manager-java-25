package service;

import marker.UnitCategory;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.service.ServiceLocator;
import ru.amster.tm.util.HashUtil;

import java.util.List;

@Ignore
public class SessionServiceTest {

    private static IServiceLocator serviceLocator;

    private static IProjectService projectService;

    private static ISessionService sessionService;

    private static IUserService userService;

    private static User user;

    private static User user1;

    private static Project project;

    private static Project project1;


    @BeforeClass
    public static void init() throws Exception {
        serviceLocator = new ServiceLocator();
        serviceLocator.getPropertyServer().init();
        serviceLocator.getEntityManagerService().initTest();
        sessionService = serviceLocator.getSessionService();
        projectService = serviceLocator.getProjectService();
        userService = serviceLocator.getUserService();
        userService.create("test", "test", Role.USER);
        userService.create("temp", "temp", Role.USER);
        user = userService.findByLogin("test");
        user1 = userService.findByLogin("temp");
        projectService.create(user.getId(), "test");
        projectService.create(user1.getId(), "temp");
        project = projectService.findOneByName(user.getId(), "test");
        project1 = projectService.findOneByName(user1.getId(), "temp");
    }

    @Test
    @Category(UnitCategory.class)
    public void signAndValidateTest() {
        String secretSession = sessionService.open("test", "test");
        String secretSession1 = sessionService.open("temp", "temp");
        Session session = sessionService.validate(secretSession);
        sessionService.validate(secretSession, Role.USER);
        Session session1 = sessionService.validate(secretSession1);
        Assert.assertEquals(session.getUser().getId(), user.getId());
        Assert.assertNotEquals(session1.getUser().getId(), user.getId());
        sessionService.clear();
    }

    @Test
    @Category(UnitCategory.class)
    public void validateTest()  {
        String secretSession = sessionService.open("test", "test");
        sessionService.validate(secretSession);
        sessionService.validate(secretSession, Role.USER);
        sessionService.clear();
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestExceptionNull()  {
        sessionService.validate(null);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException()  {
        sessionService.validate("asdasdasd12e1eadadq2d1utf");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException1() {
        String secretSession = sessionService.open("test1", "test1");
        sessionService.validate(secretSession);
    }

    @Test
    @Category(UnitCategory.class)
    public void checkDataAccessTest() {
        Assert.assertTrue(sessionService.checkDataAccess("test", "test"));
        Assert.assertTrue(sessionService.checkDataAccess("temp", "temp"));
        Assert.assertFalse(sessionService.checkDataAccess("", "demo"));
        Assert.assertFalse(sessionService.checkDataAccess("t", ""));
        Assert.assertFalse(sessionService.checkDataAccess(null, "demo"));
        Assert.assertFalse(sessionService.checkDataAccess("t", null));
        Assert.assertFalse(sessionService.checkDataAccess("test1", "demo"));
        Assert.assertFalse(sessionService.checkDataAccess("test", "demo"));
    }

    @AfterClass
    public static void exit() {
        userService.clear();
    }

}