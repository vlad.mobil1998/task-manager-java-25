package service;

import marker.UnitCategory;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.repository.UserRepository;
import ru.amster.tm.service.ServiceLocator;
import ru.amster.tm.service.UserService;
import ru.amster.tm.util.HashUtil;

@Ignore
public class UserServiceTest {

    private static IServiceLocator serviceLocator;

    private static IUserService userService;

    private static User user;

    private static User user1;

    private static User user2;

    @BeforeClass
    public static void init() throws Exception {
        serviceLocator = new ServiceLocator();
        serviceLocator.getPropertyServer().init();
        serviceLocator.getEntityManagerService().initTest();
        userService = serviceLocator.getUserService();
        IUserService userService = serviceLocator.getUserService();
        userService.create("test", "test", Role.USER);
        userService.create("temp", "temp", Role.USER);
        userService.create("test1", "test", Role.USER);
        userService.create("temp1", "temp1", Role.USER);
        userService.create("admin", "admin", Role.USER);
        userService.create("admin1", "admin1", Role.ADMIN);
        user = userService.findByLogin("test");
        user1 = userService.findByLogin("temp");
        user2 = userService.findByLogin("admin");
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdTest() {
        User userTest = userService.findById(user.getId());
        User userTest1 = userService.findById(user1.getId());
        Assert.assertEquals(user.getLogin(), userTest.getLogin());
        Assert.assertEquals(userTest1.getLogin(), user1.getLogin());
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void findByIdTestExceptionEmpty() {
        userService.findById("");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void findByIdTestExceptionNull() {
        userService.findById(null);
    }

    @Test @Category(UnitCategory.class)
    public void findByLoginTest() {
        User userTest = userService.findByLogin("test");
        Assert.assertEquals(user.getId(), userTest.getId());
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void findByLoginTestExceptionEmpty() {
        userService.findByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void findByLoginTestExceptionNull() {
        userService.findByLogin(null);
    }

    @Test @Category(UnitCategory.class)
    public void removeByIdTest() {
        userService.removeById(user2.getId());
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void removeByIdTestExceptionEmpty() {
        userService.removeById("");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void removeByIdTestExceptionNull() {
        userService.removeById(null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByLoginTest() {
        userService.removeByLogin("temp1");
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void removeByLoginTestExceptionNull() {
        userService.removeByLogin(null);
    }

    @Test
    @Category(UnitCategory.class)
    public void updateEmailTest() {
        userService.updateEmail(user2.getId(), "d@d.ru");
        Assert.assertNotEquals(user2.getEmail(), "d@d.ru");
    }

    @Test
    @Category(UnitCategory.class)
    public void updateFirstNameTest() {
        userService.updateFirstName(user2.getId(), "tempura");
        Assert.assertNotEquals(user2.getFistName(), "tempura");
    }

    @Test
    @Category(UnitCategory.class)
    public void updateLastNameTest() {
        userService.updateLastName(user2.getId(), "tempuraLN");
        Assert.assertNotEquals(user2.getLastName(), "tempuraLN");
    }

    @Test
    @Category(UnitCategory.class)
    public void updateMiddleNameTest() {
        userService.updateMiddleName(user2.getId(), "tempuraMN");
        Assert.assertNotEquals(user2.getMiddleName(), "tempuraMN");
    }

    @Test
    @Category(UnitCategory.class)
    public void updatePasswordTest() {
        userService.updatePassword(user2.getId(), "temp22");
        Assert.assertNotEquals(user2.getPasswordHash(), HashUtil.salt("temp22"));
    }

    @Test
    @Category(UnitCategory.class)
    public void lockAndUnlockUserByLoginTest() {
        userService.lockUserByLogin("test");
        User user = userService.findByLogin("test");
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin("test");
        user = userService.findByLogin("test");
        Assert.assertFalse(user.getLocked());
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void lockUserByLoginTestExceptionEmpty() {
        userService.lockUserByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void lockUserByLoginTestExceptionNull() {
        userService.lockUserByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void unlockUserByLoginTestExceptionEmpty() {
        userService.unlockUserByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void unlockUserByLoginTestExceptionNull() {
        userService.unlockUserByLogin(null);
    }

    @AfterClass
    public static void exit() {
        userService.clear();
    }

}