package service.project;

import marker.UnitCategory;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.User;
import ru.amster.tm.service.ServiceLocator;

@Ignore
public class ProjectCreateAndUpdate {

    private static IServiceLocator serviceLocator;

    private static IProjectService projectService;

    private static User user;

    private static User user1;

    private static Project project;

    private static Project project1;

    @BeforeClass
    public static void addProjectData() throws Exception{
        serviceLocator = new ServiceLocator();
        serviceLocator.getPropertyServer().init();
        serviceLocator.getEntityManagerService().initTest();
        projectService = serviceLocator.getProjectService();
        IUserService userService = serviceLocator.getUserService();
        userService.create("test", "test", Role.USER);
        userService.create("temp", "temp", Role.USER);
        user = userService.findByLogin("test");
        user1 = userService.findByLogin("temp");
        projectService.create(user.getId(), "test");
        projectService.create(user1.getId(), "temp");
        project = projectService.findOneByName(user.getId(), "test");
        project1 = projectService.findOneByName(user1.getId(), "temp");
    }

    @Test
    @Category(UnitCategory.class)
    public void createAndFindProjectTest() {
        Project project, project1;
        projectService.create(user.getId(), "134");
        projectService.create(user1.getId(), "341");
        project = projectService.findOneByName(user.getId(), "134");
        Assert.assertEquals("134", project.getName());
        Assert.assertEquals(user.getId(), project.getUser().getId());

        project1 = projectService.findOneByName(user1.getId(), "341");
        Assert.assertEquals("341", project1.getName());
        Assert.assertEquals(user1.getId(), project1.getUser().getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void updateProjectByIdTest() {
        Project project;
        projectService.create(user.getId(), "update");
        project = projectService.findOneByName(user.getId(), "update");

        projectService.updateProjectById(user.getId(), project.getId(), "testUp", "up");
        project1 = projectService.findOneByName(user.getId(), "testUp");

        Assert.assertEquals(project1.getName(), "testUp");
        Assert.assertEquals(project1.getDescription(), "up");
        Assert.assertEquals(project1.getId(), project.getId());
        Assert.assertEquals(project1.getUser().getId(), project.getUser().getId());
    }

    @AfterClass
    public static void exit() {
        serviceLocator.getUserService().clear();
    }

}